﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BasicLibrary.Models
{
    public class Vehicle
    {
        [Key]
        public int Id { get; set; }
        public string RegistrationNr { get; set; }
        public string FuelType { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public string Year { get; set; }
        public string Description { get; set; }
    }

    public enum FuelTypeEnum
    {
        Petrol, Diesel
    }
}
