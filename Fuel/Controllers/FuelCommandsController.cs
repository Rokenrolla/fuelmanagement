﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Fuel.Helper;
using Fuel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;


namespace Fuel.Controllers
{

    public class FuelCommandsController : Controller
    {
        HelpingAPI _api = new HelpingAPI();

        public async Task<IActionResult> ViewAllFuelings(string searchString)
        {
            List<BasicLibrary.Models.Fuel> fuels = new List<BasicLibrary.Models.Fuel>();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/Fuels");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                fuels = JsonConvert.DeserializeObject<List<BasicLibrary.Models.Fuel>>(result);
            }
            if (searchString != null)
            {
                return View(fuels.Where(x => x.RegistrationNr.Contains(searchString)));
            }
            else return View(fuels);
        }

        

        public async Task<IActionResult> FuelDetails(int Id)
        {
            var fuels = new BasicLibrary.Models.Fuel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Fuels/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                fuels = JsonConvert.DeserializeObject<BasicLibrary.Models.Fuel>(result);
            }
            return View(fuels);
        }




        public IActionResult CreateFuelings()
        {
            return View();
        }
       
        [HttpPost]
        public IActionResult CreateFuelings(BasicLibrary.Models.Fuel fuels)
        {
            HttpClient client = _api.Initial();
            var postTask = client.PostAsJsonAsync<BasicLibrary.Models.Fuel>("api/Fuels", fuels);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("ViewAllFuelings");
            }
            return View();
        }


        public async Task<IActionResult> FuelEdit(int Id)
        {
            var fuels = new BasicLibrary.Models.Fuel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Fuels/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                fuels = JsonConvert.DeserializeObject<BasicLibrary.Models.Fuel>(result);
            }
            return View(fuels);
        }


        [HttpPost]
        public IActionResult FuelEdit(int Id, BasicLibrary.Models.Fuel fuels)
        {
            HttpClient client = _api.Initial();
            var postTask = client.PutAsJsonAsync<BasicLibrary.Models.Fuel>($"api/Fuels/{Id}", fuels);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("ViewAllFuelings");
            }
            return View();
        }



        public async Task<IActionResult> FuelDelete(int Id)
        {
            var fuels = new BasicLibrary.Models.Fuel();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Fuels/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                fuels = JsonConvert.DeserializeObject<BasicLibrary.Models.Fuel>(result);
            }
            return View(fuels);
        }


        [HttpPost]
        public async Task<IActionResult> FuelDelete(int Id, BasicLibrary.Models.Fuel x)
        {
            HttpClient client = _api.Initial();
            await client.DeleteAsync($"api/Fuels/{Id}");
            return RedirectToAction("ViewAllFuelings");
        }

    }
}