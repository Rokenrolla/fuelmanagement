﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Fuel.Models;
using Fuel.Helper;
using System.Net.Http;
using Newtonsoft.Json;
using BasicLibrary.Models;

namespace Fuel.Controllers
{
    public class VehicleController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        HelpingAPI _api = new HelpingAPI();

        public IActionResult ViewAllVehicles(string searchString)
        {
            GetMethodAsync x = new GetMethodAsync();
            if (searchString != null)
            {
                return View(x.TryHard().Result.Where(y=>y.RegistrationNr.Contains(searchString)));
            }
            else
            {
                return View(x.TryHard().Result);
            }
        }

        public async Task<IActionResult> Details(int Id)
        {
            var vehicles = new Vehicle();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Vehicles/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                vehicles = JsonConvert.DeserializeObject<Vehicle>(result);
            }
            return View(vehicles);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create (Vehicle vehicle)
        {
            HttpClient client = _api.Initial();
            var postTask = client.PostAsJsonAsync<Vehicle>("api/Vehicles", vehicle);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("ViewAllVehicles");
            }
            return View();
        }
  

        public async Task<IActionResult> Edit(int Id)
        {
            var vehicles = new Vehicle();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Vehicles/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                vehicles = JsonConvert.DeserializeObject<Vehicle>(result);
            }
            return View(vehicles);
        }


        [HttpPost]
        public IActionResult Edit(int Id, Vehicle vehicle)
        {
            HttpClient client = _api.Initial();
            var postTask = client.PutAsJsonAsync<Vehicle>($"api/Vehicles/{Id}", vehicle);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("ViewAllVehicles");
            }
            return View();
        }



        public async Task<IActionResult> Delete(int Id)
        {
            var vehicles = new Vehicle();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Vehicles/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                vehicles = JsonConvert.DeserializeObject<Vehicle>(result);
            }
            return View(vehicles);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int Id, Vehicle x)
        {
            HttpClient client = _api.Initial();
            await client.DeleteAsync($"api/Vehicles/{Id}");
            return RedirectToAction("ViewAllVehicles");
        }     






    }
}
