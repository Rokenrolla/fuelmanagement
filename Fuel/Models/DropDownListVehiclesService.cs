﻿using BasicLibrary.Models;
using Fuel.Controllers;
using Fuel.Helper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fuel.Models
{

    public class DropDownListVehicles
    {
        public List<string> GetVechiclesAsync()
        {
            GetMethodAsync x = new GetMethodAsync();
            List<Vehicle> y = new List<Vehicle>();
            y = x.TryHard().Result;
            return y.Select(x => x.RegistrationNr).ToList();
        }
    }
}
