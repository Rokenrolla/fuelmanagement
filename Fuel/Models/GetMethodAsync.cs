﻿using BasicLibrary.Models;
using Fuel.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fuel.Models
{
    public class GetMethodAsync
    {
        HelpingAPI _api = new HelpingAPI();
        public async Task<List<Vehicle>> TryHard()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/Vehicles");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(result);
            }
            return vehicles;
        }     
    }
}
