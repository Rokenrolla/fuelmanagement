﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Threading.Tasks;

//namespace FuelApi.Models
//{
//    public class Fuel
//    {
//        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
//        public int Id { get; set; }
//        public string RegistrationNr { get; set; }
//        [ForeignKey("RegistrationNr")]
//        public string DateOfFueling { get; set; }
//        public decimal Quantity { get; set; }
//        public decimal TotalPrice { get; set; }
//        public string Area { get; set; }
//        public string Description { get; set; }
//    }
//}
